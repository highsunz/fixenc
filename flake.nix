{
  description = "Fix encodings (of the containing files and the ZIP) of uploaded ZIP archive";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  nixConfig = {
    # extra-substituters = [ "https://cache.blurgy.xyz" ];
    # extra-trusted-public-keys = [ "cache.blurgy.xyz:Xg9PvXkUIAhDIsdn/NOUUFo+HHc8htSiGj7O6fUj/W4=" ];
  };

  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      config = {
        # allowUnfree = true;
        # cudaSupport = true;
      };
      overlays = [
        self.overlays.default
      ];
    };
    inherit (nixpkgs) lib;
  in {
    packages = rec {
      inherit (pkgs) fixenc-backend fixenc-frontend fixenc;
      default = fixenc;
    };
    devShells = rec {
      default = pureShell;
      pureShell = pkgs.mkShell {
        buildInputs = [
        ];
        shellHook = ''
          [[ "$-" == *i* ]] && exec "$SHELL"
        '';
      };
      impureShell = pureShell.overrideAttrs (o: {
        shellHook = ''
          source <(sed -Ee '/\$@/d' ${lib.getExe pkgs.nixgl.nixVulkanIntel})
          source <(sed -Ee '/\$@/d' ${lib.getExe pkgs.nixgl.nixGLIntel})
          source <(sed -Ee '/\$@/d' ${lib.getExe pkgs.nixgl.auto.nixGLNvidia}*)
        '' + o.shellHook or "";
      });
    };
  }) // {
    overlays.default = final: prev: let
      version = "0.1.0";
    in rec {
      fixenc-frontend = final.callPackage ./frontend { inherit version; };
      fixenc-backend = final.callPackage ./backend { inherit version; };
      fixenc = final.writeShellScript "fixenc" ''
        set -Eeuo pipefail

        cd ${fixenc-frontend}/share/webapps/fixenc
        ${fixenc-backend}/bin/fixenc-backend
      '';
    };
    hydraJobs = self.packages;
  };
}
