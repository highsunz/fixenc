import { useState } from 'react'
import './App.css'

type ApplicationStatus = 'idle' | 'uploading' | 'processing' | 'downloading'

// Simple application that contains only two buttons and a status area.
// On click of the first button, it opens a file chooser to select ZIP archives from user's
// computer.
// Once the ZIP file is uploaded to the backend, the backend will process it and return a new ZIP
// archive.  The second button will download the processed ZIP archive.
// The status area will display the current status of the application.
// The two buttons should occpy the full width of the page, the first being green, and the second
// being gray if there is nothing to download at this time, and blue if the ZIP archive is ready to
// be downloaded.
function App() {
  const [downloadUrl, setDownloadUrl] = useState<string | null>(null)
  const [status, setStatus] = useState<ApplicationStatus>('idle')
  const [progressPercentage, setProgressPercentage] = useState(0)

  const formatStatus = (status: ApplicationStatus) => {
    switch (status) {
      case 'idle':
        return '空闲'
      case 'uploading':
        return `上传中 ${progressPercentage}%`
      case 'processing':
        return '处理中'
      case 'downloading':
        return `下载中 ${100 - progressPercentage}%`
    }
  }

  const upload = async () => {
    const fileInput = document.createElement('input')
    fileInput.type = 'file'
    fileInput.accept = '.zip'
    fileInput.onchange = async () => {
      if (fileInput.files) {
        // const formData = new FormData()
        // formData.append('file', fileInput.files[0])
        // const response = await fetch('/fix', {
        //   method: 'POST',
        //   body: formData,
        // })

        // progress bar monitoring upload progress
        const formData = new FormData()
        formData.append('file', fileInput.files[0])
        const xhr = new XMLHttpRequest()
        xhr.upload.onprogress = (e) => {
          if (e.lengthComputable) {
            const percent = Math.round((e.loaded / e.total) * 100)
            setProgressPercentage(percent)
            if (percent === 100) {
              setStatus("processing")
            }
          }
        }
        // and download progress bar
        xhr.onprogress = (e) => {
          if (e.lengthComputable) {
            const percent = Math.round((e.loaded / e.total) * 100)
            setProgressPercentage(100 - percent)
            setStatus("downloading")
            if (percent === 100) {
              setStatus("idle")
            }
          }
        }

        // the response from the xhr request
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              // console.log(xhr.responseText)
              const blob = xhr.response
              setDownloadUrl(window.URL.createObjectURL(blob))
            } else {
              alert('处理失败，文件可能有解压密码保护，或上传的不是压缩文件')
            }
          }
        }

        xhr.open('POST', '/fix')
        xhr.responseType = 'blob'
        xhr.send(formData)
        setStatus("uploading")
        setDownloadUrl(null)
      }
    }
    fileInput.click()
  }
  const download = async () => {
    if (downloadUrl) {
      const response = await fetch(downloadUrl)
      const blob = await response.blob()
      const url = window.URL.createObjectURL(blob)
      const a = document.createElement('a')
      a.href = url
      a.download = 'processed.zip'
      a.click()
    }
  }

  // tooltip
  const [showTooltip, setShowTooltip] = useState(false);
  const [tooltipPos, setTooltipPos] = useState({ x: 0, y: 0 });

  const hideTooltip = () => { setShowTooltip(false); }
  const unhideTooltip = () => { setShowTooltip(true); }
  const updateTooltipPos = (e: React.MouseEvent<HTMLSpanElement>) => {
    const x = e.clientX;
    const y = e.clientY;
    unhideTooltip();
    setTooltipPos({ x, y });
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="App-header-content">
          <h1>
            <a href=''>📦🔧</a>
          </h1>
          <p>
            修复 ZIP 包中的文件名、汉字内容编码、文件夹结构等问题
          </p>
          <div className="buttons-container">
            <button className="upload-button" onClick={upload}>
              上传
            </button>
            <span
              onMouseMove={updateTooltipPos}
              onMouseEnter={unhideTooltip}
              onMouseLeave={hideTooltip}
            >
            <button
              className="download-button"
              onClick={download}
              disabled={!downloadUrl}
            >
              保存
            </button>
            </span>

            {showTooltip && !downloadUrl && (
              <div
                className="button-tooltip"
                style={{
                  position: "fixed",
                  left: tooltipPos.x + 10,
                  top: tooltipPos.y + 10,
                }}
              >
                "保存"按钮将在上传后自动启用
              </div>
            )}

          </div>

          {status !== "idle" && (<div className="status-container">
            <progress
              style={{
                direction: status === "downloading" ? "rtl" : "ltr",
              }}
              rev=''
              title='abc'
              value={progressPercentage}
              max={100}
            />
            <p className='status-text'>{formatStatus(status)}</p>
          </div>)}
        </div>
      </header>
    </div>
  )
}

export default App
