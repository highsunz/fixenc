{ version, buildNpmPackage }:

buildNpmPackage {
  pname = "fixenc-frontend";
  inherit version;
  src = ./.;

  npmDepsHash = "sha256-uaYU6IZp7WR3zmuC9gaGLWmXRkrLsyDY1xeC77JgOv4=";

  buildPhase = ''
    dest="$out/share/webapps/fixenc"
    mkdir -p "$(dirname "$dest")"
    npm run build
    cp -vr dist "$dest"
  '';

  shellHook = ''
    [[ "$-" == *i* ]] && exec "$SHELL"
  '';
}
