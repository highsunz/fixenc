#!/usr/bin/env python3

from os import chdir
from pathlib import Path
import subprocess
import sys
import tempfile
from typing import Tuple
import zipfile

from flask import Flask, abort, request, send_file


def get_nth_parent(file: Path, n: int) -> Path:
    if n == 0:
        return file
    return get_nth_parent(file.parent, n - 1)


def flatten_directory(dir: Path, depth: int=0):
    target_dir = get_nth_parent(dir, depth)
    for file in dir.iterdir():
        if file.is_dir():
            flatten_directory(file, depth + 1)
        else:
            file.rename(target_dir.joinpath(file.name))


def fix_file_content(file: Path) -> Tuple[str, None]:
    # REF: <https://docs.python.org/3.11/library/codecs.html#standard-encodings>
    chn_encodings = (
        "big5",
        "big5hkscs",
        "cp950",
        "hz",
        "iso2022_jp_2",
        "utf-8",
        "gb18030",
        "gbk",
        "gb2312",
    )
    ## below encodings are only known to iconv; python does not recognize them
    # "gb",
    # "gb_198880",
    # "gb_1988-80",
    # "gb13000",
    content, encoding_in_use = "", None
    for encoding in chn_encodings:
        try:
            with open(file, encoding=encoding, mode="r") as f:
                content = f.read()
                encoding_in_use = encoding
                break
        except UnicodeDecodeError as e:
            pass
        except Exception as e:
            print("unhandled error:", e, file=sys.stderr)
    return content, encoding_in_use



def fix_zip_file(zip_path: Path) -> Path:
    zip_path = zip_path.absolute()
    with tempfile.TemporaryDirectory() as outdir:
        outdir = Path(outdir)
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpdir = Path(tmpdir)
            subprocess.run(["unar", "-o", tmpdir, zip_path], check=True)
            flatten_directory(tmpdir)
            for file in tmpdir.iterdir():
                if file.is_dir():
                    continue
                content, encoding = fix_file_content(file)
                if encoding is None:
                    print("Can't find encoding for {}".format(file), file=sys.stderr)
                    continue
                print("'{}' uses '{} encoding".format(file, encoding))
                with open(outdir.joinpath(file.name), encoding="utf-8", mode="w") as f:
                    f.write(content)
        chdir(outdir.parent)
        out_path = zip_path.absolute().with_suffix(".fixed.zip")
        with zipfile.ZipFile(out_path, "w", zipfile.ZIP_DEFLATED, 9) as zip:
            for file in outdir.iterdir():
                zip.write(file, file.name)
        return out_path


if __name__ == "__main__":
    app = Flask(__name__)
    cwd = Path(app.instance_path).parent
    @app.route("/")
    def index():
        return send_file(cwd.joinpath("index.html"))
    # default route that serves files, index.html as default
    @app.route("/<path:path>")
    def send(path):
        if not cwd.joinpath(path).exists():
            abort(404)
        return send_file(cwd.joinpath(path))
    @app.route("/fix", methods=["POST"])
    def fix():
        if len(request.files) != 1:
            abort(400)
        file = request.files["file"]
        if file.filename == "":
            abort(403)
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpdir = Path(tmpdir)
            file.save(tmpdir.joinpath(file.filename))
            file = tmpdir.joinpath(file.filename)
            fixed_file = fix_zip_file(file)
            return send_file(fixed_file)
    app.run(port=4857)
