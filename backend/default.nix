{ version, python3, stdenvNoCC
, unar
, makeWrapper
}:

stdenvNoCC.mkDerivation {
  pname = "fixenc-backend";
  inherit version;
  src = ./main.py;

  buildInputs = [
    (python3.withPackages (pp: with pp; [
      flask
    ]))
    makeWrapper
  ];

  dontUnpack = true;

  installPhase = ''
    install -Dvm555 $src $out/bin/fixenc-backend
  '';

  postFixup = ''
    patchShebangs --build $out/bin/fixenc-backend
    wrapProgram $out/bin/fixenc-backend \
      --prefix PATH : ${unar}/bin
  '';

  shellHook = ''
    [[ "$-" == *i* ]] && exec "$SHELL"
  '';
}
